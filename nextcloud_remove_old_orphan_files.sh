#!/bin/bash

ROOTDIR=/var/www/html/nextcloud
WWWUSER=www-data
APPLIST=$(ls ${ROOTDIR}/apps)

rmflag=""
read -p 'Do you want to confirm before removing a file (-i) option of an interactive rm command ?[yes/no]' interactive
if [[ "${interactive}" == "yes" ]]; then
  rmflag=" -i "
fi

echo -e "\n######################## Checking old orphan files for Nextcloud core ###########################\n"
checkcore=$(sudo -u ${WWWUSER} php ${ROOTDIR}/occ integrity:check-core)
if [ -n "${checkcore}" ]; then
  echo -e ${checkcore}|sed -e "s/ - /\n/g" |grep --color expected -B1
  read -p "Do you want do remove these files ?[yes/no]" answer
  if [[ "${answer}" == "yes" ]]; then
    file_list=$(sudo -u ${WWWUSER} php ${ROOTDIR}/occ integrity:check-core |sed -e "s/- EXTRA_FILE://"  |gawk -F "[ :]" 'RS="\n      -", ORS=" " {print;}' |tr -s '[:space:]'  |gawk -F"[ :]" '{if ($7 == "current") {print $3;}}')
    for file in $(echo ${file_list}); do
      rm ${rmflag} ${ROOTDIR}/${file}
    done
  fi
fi


echo -e "\n######################## Checking old orphan files for Nextcloud apps ###########################\n"
for app in ${APPLIST}; do
  cd ${ROOTDIR}/apps/${app}
  checkapp=$(sudo -u ${WWWUSER} php ${ROOTDIR}/occ integrity:check-app ${app})
  if [ -n "${checkapp}" ]; then
    echo -e ${checkapp}|sed -e "s/ - /\n/g" |grep --color expected -B1
    read -p "Do you want do remove these files ?[yes/no]" answer
    if [[ "${answer}" == "yes" ]]; then
      file_list=$(sudo -u ${WWWUSER} php ${ROOTDIR}/occ integrity:check-app ${app} |sed -e "s/- EXTRA_FILE://"  |gawk -F "[ :]" 'RS="\n      -", ORS=" " {print;}' |tr -s '[:space:]'  |gawk -F"[ :]" '{if ($7 == "current") {print $3;}}')
      for file in $(echo ${file_list}); do
        rm ${rmflag} ${ROOTDIR}/apps/${app}/${file}
      done
    fi
  fi
done
